### Integration script for client: El Tunel

## Introduction

This script was created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. El Tunel is a client that mainly sends their data through an ERP called Opus, that has a SOAP API to expose endpoints that Sagal can request from.

## Workflow

- Article data is retrieved from the client's ERP.
- The data is restructured and processed for each article, being transformed to a Sagal-friendly format. Special considerations are taken to ensure the availability of the stock for some products.
- The restructured data is dispatched to Sagal.

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number.
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number.
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **OPUS_PATH/OPUS_USERNAME/OPUS_HASH/OPUS_ENTERPRISE/OPUS_TERMINAL**: Parameters that are necessary to make the request to the client's ERP.

## Functions

### getAllArticlesFromOpus()

**Description:** retrieves El Tunel's article data from the Opus ERP, and returns a json object containing all retrieved articles.

### processAllArticles(Array articlesList)

**Description:** iterates the article list and for every article calls a function to process the article.

### processArticle(Object article)

**Description:** Processes the article by restructuring its data and dispatching it to Sagal. It is verified if the article has an existing valid discount to be applied as an offer price, or if the stock availability needs to be consulted on a per-store basis rather than calculated as the sum total of all stocks.

## Constraints

- If the sum total of the article's stock is less than 3 but higher than 0, then the article's stock needs to be consulted on a per-store basis according to the store code listen in the article's data.
