import xml2js from "https://esm.sh/xml2js?pin=v55&deno-std=0.116.0";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const user = Deno.env.get("OPUS_USERNAME");
const hash = Deno.env.get("OPUS_HASH");
const opusPath = Deno.env.get("OPUS_PATH");
const enterprise = Deno.env.get("OPUS_ENTERPRISE");
const terminal = Deno.env.get("OPUS_TERMINAL");
const partial = Deno.env.get("PARTIAL");

//Prices can receive a discount (get as Setting)
try {
  console.log("ELTUNEL -> fetch articles");
  let parsedArticles = await getAllArticlesFromOpus();
  await processAllArticles(parsedArticles["Products"]["Product"]);
} catch (e) {
  console.log(`${e}, ${e.message}`);
}

async function getAllArticlesFromOpus() {
  let payload = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://comunicacionopus.newj.opus/">
                    <soapenv:Header/>
                    <soapenv:Body>
                      <com:getProducto>
                          <opusPath type='xsd:string'>${opusPath}</opusPath>
                          <empresa type='xsd:string'>${enterprise}</empresa>
                          <user type='xsd:string'>${user}</user>
                          <hash type='xsd:string'>${hash}</hash>
                          <terminal type='xsd:string'>${terminal}</terminal>
                          <xml type="xsd:string"></xml>
                      </com:getProducto>
                    </soapenv:Body>
                  </soapenv:Envelope>`;

  let response = await fetch(
    `http://190.64.143.58:8080/ComunicacionOpus/ComunicacionOpus/ComunicacionOpus_WS`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/xml",
      },
      body: payload,
    }
  );

  let textResponse = await response.text();

  let parser = new xml2js.Parser();
  let parsedResponse = await parser.parseStringPromise(textResponse);
  let returnStatement =
    parsedResponse["soap:Envelope"]["soap:Body"][0][
      "ns2:getProductoResponse"
    ][0].return[0];
  let parsedReturn = await parser.parseStringPromise(returnStatement);

  return parsedReturn;
}

async function processAllArticles(articlesList) {
  let payloads = [];
  for (let article of articlesList) {
    let payload = processArticle(article);
    payloads.push(payload);
  }
  await sagalDispatchBatch({
    products: payloads,
    batch_size: 100,
  });
}

function getStock(article) {
  let stock = 0;
  for (let stockData of article["Stocks"][0]["Stock"]) {
    stock += Number(stockData["$"]["StrgStock"]);
  }
  stock = stock > 0 ? stock : 0;
  return stock;
}

function processArticle(article) {
  let sku = article["$"].ProdId;
  let name = article["$"].ProdDscWeb
    ? article["$"].ProdDscWeb
    : article["$"].ProdDsc;
  let active = Boolean(article["$"].Activo);
  let storeCode = article["$"].ProdTienda;
  let price = {
    value: Math.round(Number(article["$"].ListaUno)),
    currency: article["$"].ProdMonId == "00" ? "$" : "U$S",
  };

  let metadata = {
    store: storeCode,
  };
  let stock = !active ? 0 : getStock(article);
  if (
    stock < 3 &&
    stock > 0 &&
    storeCode != "B"
  ) {
    stock = 0;
  }

  let articlePayload = {
    sku: sku,
    client_id: clientId,
    integration_id: clientIntegrationId,
    options: {
      merge: false,
      partial: partial === "true",
    },
    ecommerce: [],
  };
  if (storeCode === "F" || storeCode == "A") {
    articlePayload.ecommerce.push({
      ecommerce_id: clientEcommerce["ELTUNEL_FARMACIA"],
      variants: [],
      properties: [
        { name: name },
        { stock: stock },
        { price: price },
        { metadata: metadata },
      ],
    });
  }

  if (storeCode === "B" || storeCode == "A") {
    articlePayload.ecommerce.push({
      ecommerce_id: clientEcommerce["ELTUNEL_BEBES"],
      variants: [],
      properties: [
        { name: name },
        { stock: stock },
        { price: price },
        { metadata: metadata },
      ],
    });
  }

  return articlePayload;
}
